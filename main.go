package main

import (
	"fmt"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/text/gregex"
	"strings"
	"test/douyin"
)

func main() {
	//t.Log("测试正常的抖音视频链接")
	url := "https://v.douyin.com/JMyLw4U/" // https://v.douyin.com/JMyLw4U/  https://v.douyin.com/JNhucQF/  https://www.iesdouyin.com/share/video/6857410503656967427/?region=CN
	//input := bufio.NewScanner(os.Stdin)
	fmt.Println("请输入需要下载的视频链接")
	//scanner := bufio.NewScanner(os.Stdin)
	//for scanner.Scan() {
	//	url = scanner.Text()
	//	fmt.Printf(url)
	//	break
	//}
	//url = scanner.Text()
	videoLink, err := douyin.WatermarkRemover(url)

	if err != nil {
		fmt.Println("pass")
	}
	if strings.Contains(videoLink, ".ixigua.com") == false {
		fmt.Println("link：  出错了")
	}
	download(videoLink)
}

func download(url string) {
	c := g.Client()
	c.SetHeaderRaw(`
			Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
			Accept-Encoding: gzip, deflate
			Accept-Language: zh-CN,zh;q=0.9
			Connection: keep-alive
			Host: v92-dy.ixigua.com
			Upgrade-Insecure-Requests: 1
			User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36
	`)
	//c.SetHeader("Referer",url)
	if r,err := c.Get(url) ; err != nil {
		panic(err)
	} else {
		defer r.Close()
		//if len(r.ReadAll())>1024{
		//	gfile.PutBytes("C:\\Users\\Lenovo\\Downloads\\视频下载去水印工具\\ee.mp4", r.ReadAll())
		//}else{
			todownload(url)
			return
		//}
	}
	fmt.Println("下载完成 ：",url)
}

func todownload(url string) {
	c := g.Client()
	c.SetHeaderRaw(`
			Accept: */*
			Accept-Encoding: identity;q=1, *;q=0
			Accept-Language: zh-CN,zh;q=0.9
			Connection: keep-alive
			Host: v92-dy.ixigua.com
			Range: bytes=0-
			User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36
	`)
	c.SetHeader("Referer",url)
	c.SetHeader("Host",Regxp(url))
	if r,err := c.Get(url) ; err != nil {
		panic(err)
	} else {
		defer r.Close()
		if len(r.ReadAll())>0{
			gfile.PutBytes("C:\\Users\\Lenovo\\Downloads\\视频下载去水印工具\\ee.mp4", r.ReadAll())
			fmt.Println("下载完成 ：",url)
		}else{
			fmt.Println("下载错误 ：",url)
		}
	}
}

func Regxp(str string)string{
	array, _ := gregex.MatchString(`"://(.*?)com/"`, str)
	if len(array)>2{
		return array[1]
	}
	return ""
}